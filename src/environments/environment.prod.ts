/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
export const environment = {
  production: true,
  onesignal: {
    appId: '74a15980-c75a-4155-ba4b-e6761c5f39e1',
    googleProjectNumber: '340660990914'
  },
};
