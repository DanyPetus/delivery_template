/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settimgs',
  templateUrl: './settimgs.page.html',
  styleUrls: ['./settimgs.page.scss'],
})
export class SettimgsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
